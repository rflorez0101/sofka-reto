
import random

jugador_activo = True
puntaje = 0
nivel_final = 5
nivel_actual = 1

if jugador_activo == True:
    for i in range(nivel_final):
        if nivel_actual > nivel_final:
            print("final del juego")
        else:
            class Respuesta:
                def __init__(self, numero_respuesta, texto_respuesta, es_correcta):
                    self.numero_respuesta = numero_respuesta
                    self.texto_respuesta = texto_respuesta
                    self.es_correcta = es_correcta

            class Pregunta:
                def __init__(self, nivel, categoria, numero_pregunta, texto_pregunta, respuestas):
                    self.nivel = nivel
                    self.categoria = categoria
                    self.numero_pregunta = numero_pregunta
                    self.texto_pregunta = texto_pregunta
                    self.respuestas = respuestas

            class Jugador:
                def __init__(self, nombre, puntaje):
                    self.nombre = nombre
                    self.puntaje = puntaje

            class Premio:
                def __init__(self, nivel, puntos):
                    self.nivel = nivel
                    self.puntos = puntos

            # Array respuestas

            respuestas_n1_p1 = []
            respuestas_n1_p1.append(Respuesta(1, 'Tres', False))
            respuestas_n1_p1.append(Respuesta(2, 'Cuatro', False))
            respuestas_n1_p1.append(Respuesta(3, 'Siete', True))
            respuestas_n1_p1.append(Respuesta(4, 'Seis', False))
            respuestas_n1_p2 = []
            respuestas_n1_p2.append(Respuesta(1, 'Uno', False))
            respuestas_n1_p2.append(Respuesta(2, 'Cinco', True))
            respuestas_n1_p2.append(Respuesta(3, 'Diez', False))
            respuestas_n1_p2.append(Respuesta(4, 'Quince', False))
            respuestas_n1_p3 = []
            respuestas_n1_p3.append(Respuesta(1, 'Nose', True))
            respuestas_n1_p3.append(Respuesta(2, 'Nouse', False))
            respuestas_n1_p3.append(Respuesta(3, 'Nuse', False))
            respuestas_n1_p3.append(Respuesta(4, 'Nase', False))
            respuestas_n1_p4 = []
            respuestas_n1_p4.append(Respuesta(1, 'China', False))
            respuestas_n1_p4.append(Respuesta(2, 'Rusia', True))
            respuestas_n1_p4.append(Respuesta(3, 'Europa', False))
            respuestas_n1_p4.append(Respuesta(4, 'Polo Sur', False))
            respuestas_n1_p5 = []
            respuestas_n1_p5.append(Respuesta(1, 'Gótico', True))
            respuestas_n1_p5.append(Respuesta(2, 'Neoclásico', False))
            respuestas_n1_p5.append(Respuesta(3, 'Barroco', False))
            respuestas_n1_p5.append(Respuesta(4, 'Brutalista', False))
            respuestas_n2_p1 = []
            respuestas_n2_p1.append(Respuesta(1, 'Amarillo', False))
            respuestas_n2_p1.append(Respuesta(2, 'Blanco', False))
            respuestas_n2_p1.append(Respuesta(3, 'Negro', False))
            respuestas_n2_p1.append(Respuesta(4, 'Verde', True))
            respuestas_n2_p2 = []
            respuestas_n2_p2.append(
                Respuesta(1, 'Multiplicar por tres', False))
            respuestas_n2_p2.append(Respuesta(2, 'Dividir entre tres', True))
            respuestas_n2_p2.append(Respuesta(3, 'Restar tres', False))
            respuestas_n2_p2.append(Respuesta(4, 'Modulo tres', False))
            respuestas_n2_p3 = []
            respuestas_n2_p3.append(Respuesta(1, 'Cercano', False))
            respuestas_n2_p3.append(Respuesta(2, 'Próximo', False))
            respuestas_n2_p3.append(Respuesta(3, 'Hace', True))
            respuestas_n2_p3.append(Respuesta(4, 'Lejano', False))
            respuestas_n2_p4 = []
            respuestas_n2_p4.append(Respuesta(1, 'Italia', True))
            respuestas_n2_p4.append(Respuesta(2, 'Francia', False))
            respuestas_n2_p4.append(Respuesta(3, 'España', False))
            respuestas_n2_p4.append(Respuesta(4, 'Argentina', False))
            respuestas_n2_p5 = []
            respuestas_n2_p5.append(Respuesta(1, 'Mareufure', False))
            respuestas_n2_p5.append(Respuesta(2, 'Trafumo', False))
            respuestas_n2_p5.append(Respuesta(3, 'Marsellesa', True))
            respuestas_n2_p5.append(Respuesta(4, 'Damatrug', False))
            respuestas_n3_p1 = []
            respuestas_n3_p1.append(Respuesta(1, '8', True))
            respuestas_n3_p1.append(Respuesta(2, '6', False))
            respuestas_n3_p1.append(Respuesta(3, '4', False))
            respuestas_n3_p1.append(Respuesta(4, '10', False))
            respuestas_n3_p2 = []
            respuestas_n3_p2.append(Respuesta(1, 'Heptágono', True))
            respuestas_n3_p2.append(Respuesta(2, 'Hexágono', False))
            respuestas_n3_p2.append(Respuesta(3, 'Septágono', False))
            respuestas_n3_p2.append(Respuesta(4, 'Sevtagoo', False))
            respuestas_n3_p3 = []
            respuestas_n3_p3.append(Respuesta(1, '10', False))
            respuestas_n3_p3.append(Respuesta(2, '21', False))
            respuestas_n3_p3.append(Respuesta(3, '19', False))
            respuestas_n3_p3.append(Respuesta(4, '24', True))
            respuestas_n3_p4 = []
            respuestas_n3_p4.append(Respuesta(1, 'Nilo', False))
            respuestas_n3_p4.append(Respuesta(2, 'Amazonas', True))
            respuestas_n3_p4.append(Respuesta(3, 'Yangtsé', False))
            respuestas_n3_p4.append(Respuesta(4, 'Misisipi ', False))
            respuestas_n3_p5 = []
            respuestas_n3_p5.append(Respuesta(1, '1945', True))
            respuestas_n3_p5.append(Respuesta(2, '1942', False))
            respuestas_n3_p5.append(Respuesta(3, '1943', False))
            respuestas_n3_p5.append(Respuesta(4, '1944', False))
            respuestas_n4_p1 = []
            respuestas_n4_p1.append(Respuesta(1, 'mamíferos', True))
            respuestas_n4_p1.append(Respuesta(2, 'anfibios', False))
            respuestas_n4_p1.append(Respuesta(3, 'reptiles', False))
            respuestas_n4_p1.append(Respuesta(4, 'peces', False))
            respuestas_n4_p2 = []
            respuestas_n4_p2.append(Respuesta(1, '10 metros', False))
            respuestas_n4_p2.append(Respuesta(2, '1 metro', False))
            respuestas_n4_p2.append(Respuesta(3, '100 metros', True))
            respuestas_n4_p2.append(Respuesta(4, '1000 metros', False))
            respuestas_n4_p3 = []
            respuestas_n4_p3.append(Respuesta(1, '20', False))
            respuestas_n4_p3.append(Respuesta(2, '23', False))
            respuestas_n4_p3.append(Respuesta(3, '21', True))
            respuestas_n4_p3.append(Respuesta(4, '26', False))
            respuestas_n4_p4 = []
            respuestas_n4_p4.append(Respuesta(1, 'Pacífico', True))
            respuestas_n4_p4.append(Respuesta(2, 'Atlántico', False))
            respuestas_n4_p4.append(Respuesta(3, 'índico', False))
            respuestas_n4_p4.append(Respuesta(4, 'Ártico', False))
            respuestas_n4_p5 = []
            respuestas_n4_p5.append(Respuesta(1, 'Japón', False))
            respuestas_n4_p5.append(Respuesta(2, 'Alemania', False))
            respuestas_n4_p5.append(Respuesta(3, 'Grecia', True))
            respuestas_n4_p5.append(Respuesta(4, 'Rusia', False))
            respuestas_n5_p1 = []
            respuestas_n5_p1.append(Respuesta(1, 'verde, blanco y rojo', True))
            respuestas_n5_p1.append(Respuesta(2, 'verde, rosa y rojo', False))
            respuestas_n5_p1.append(
                Respuesta(3, 'negro, blanco y rojo', False))
            respuestas_n5_p1.append(
                Respuesta(4, 'verde, blanco y café', False))
            respuestas_n5_p2 = []
            respuestas_n5_p2.append(Respuesta(1, '58', False))
            respuestas_n5_p2.append(Respuesta(2, '72', False))
            respuestas_n5_p2.append(Respuesta(3, '63', True))
            respuestas_n5_p2.append(Respuesta(4, '54', False))
            respuestas_n5_p3 = []
            respuestas_n5_p3.append(Respuesta(1, 'been', True))
            respuestas_n5_p3.append(Respuesta(2, 'bean', False))
            respuestas_n5_p3.append(Respuesta(3, 'buon', False))
            respuestas_n5_p3.append(Respuesta(4, 'beag', False))
            respuestas_n5_p4 = []
            respuestas_n5_p4.append(Respuesta(1, 'Rusia', False))
            respuestas_n5_p4.append(Respuesta(2, 'China', True))
            respuestas_n5_p4.append(Respuesta(3, 'India', False))
            respuestas_n5_p4.append(Respuesta(4, 'Africa', False))
            respuestas_n5_p5 = []
            respuestas_n5_p5.append(Respuesta(1, 'Leonardo da Vinci', True))
            respuestas_n5_p5.append(Respuesta(2, 'Pablo Picasso', False))
            respuestas_n5_p5.append(Respuesta(3, 'Vincent van Gogh', False))
            respuestas_n5_p5.append(Respuesta(4, 'Rembrandt', False))

            # Array premios

            premios = []
            premios.append(Premio(1, 10))
            premios.append(Premio(2, 20))
            premios.append(Premio(3, 30))
            premios.append(Premio(4, 40))
            premios.append(Premio(5, 50))

            # Array preguntas
            lista_preguntas = [
                Pregunta(1, 'Cultura general', 1,
                         '¿Cuántos colores tiene un arcoiris?', respuestas_n1_p1),
                Pregunta(1, 'Matematicas', 2,
                         '¿Qué valor expresa el número romano V ?', respuestas_n1_p2),
                Pregunta(1, 'Inglés', 3,
                         '¿Cómo se dice nariz en inglés?', respuestas_n1_p3),
                Pregunta(1, 'Geografía', 4,
                         '¿Cuál es el país más grande del mundo?', respuestas_n1_p4),
                Pregunta(1, 'Historia', 5,
                         '¿De qué estilo arquitectónico es la Catedral de Notre Dame en París?', respuestas_n1_p5),
                Pregunta(2, 'Cultura general', 1,
                         '¿Cuál es el color que representa la esperanza?', respuestas_n2_p1),
                Pregunta(2, 'Matematicas', 2,
                         'Para calcular cuánto es un tercio de 3996, ¿qué tienes que hacer?', respuestas_n2_p2),
                Pregunta(2, 'Inglés', 3,
                         '¿ qué significa la palabra "ago" en inglés ?', respuestas_n2_p3),
                Pregunta(2, 'Geografía', 4,
                         '¿Qué país tiene forma de bota?', respuestas_n2_p4),
                Pregunta(2, 'Historia', 5,
                         '¿Cómo se llama el himno nacional de Francia?', respuestas_n2_p5),
                Pregunta(3, 'Cultura general', 1,
                         '¿Cuántas patas tiene la araña?', respuestas_n3_p1),
                Pregunta(3, 'Matematicas', 2,
                         '¿Cómo se llama el polígono de siete lados?', respuestas_n3_p2),
                Pregunta(3, 'Inglés', 3,
                         '¿Cuántos tiempos verbales hay en inglés?', respuestas_n3_p3),
                Pregunta(3, 'Geografía', 4,
                         '¿Cuál es el río más largo del mundo?', respuestas_n3_p4),
                Pregunta(3, 'Historia', 5,
                         '¿En qué año acabó la II Guerra Mundial?', respuestas_n3_p5),
                Pregunta(4, 'Cultura general', 1,
                         '¿Qué tipo de animal es la ballena?', respuestas_n4_p1),
                Pregunta(4, 'Matematicas', 2,
                         '¿Cuántos metros es un hectómetro?', respuestas_n4_p2),
                Pregunta(4, 'Inglés', 3,
                         '¿ Cuántas consonantes tiene el abacedario en inglés ?', respuestas_n4_p3),
                Pregunta(4, 'Geografía', 4,
                         '¿Cuál es el océano más grande?', respuestas_n4_p4),
                Pregunta(4, 'Historia', 5,
                         '¿Dónde originaron los juegos olímpicos?', respuestas_n4_p5),
                Pregunta(5, 'Cultura general', 1,
                         '¿De qué colores es la bandera de México?', respuestas_n5_p1),
                Pregunta(5, 'Matematicas', 2,
                         '¿Cuál es el resultado de multiplicar 7x9?', respuestas_n5_p2),
                Pregunta(5, 'Inglés', 3,
                         '¿ Cuál es el pasado participio del verbo be en inglés?', respuestas_n5_p3),
                Pregunta(5, 'Geografía', 4,
                         '¿Cuál es país más poblado de la Tierra?', respuestas_n5_p4),
                Pregunta(5, 'Historia', 5, '¿Quién pintó “la última cena”?', respuestas_n5_p5), ]


            pregunta_nivel_iterator = filter(
                lambda pregunta_nivel: pregunta_nivel.nivel == nivel_actual, lista_preguntas)
            filtronivel = list(pregunta_nivel_iterator)

            # Prueba de filtro nivel
            # print('Listado de preguntas nivel ', filtronivel[0].nivel,'\n')
            # print(filtronivel[0].numero_pregunta,filtronivel[0].texto_pregunta)
            # print(filtronivel[1].numero_pregunta,filtronivel[1].texto_pregunta)
            # print(filtronivel[2].numero_pregunta,filtronivel[2].texto_pregunta)
            # print(filtronivel[3].numero_pregunta,filtronivel[3].texto_pregunta)
            # print(filtronivel[4].numero_pregunta,filtronivel[4].texto_pregunta)

            pregunta_aleatoria_iterator = filter(
                lambda filtronivel: filtronivel.numero_pregunta == random.randint(1, 5), lista_preguntas)
            filtroaleatorio = list(pregunta_aleatoria_iterator)

            # Prueba de filtro aleatorio
            # print('\n Pregunta seleccionada aleatoriamente \n' )

            print('Nivel -', filtroaleatorio[0].nivel, '\n')
            print(filtroaleatorio[0].texto_pregunta)
            for r in filtroaleatorio[0].respuestas:
                print(r.numero_respuesta, r.texto_respuesta)
            a = int(input('\ningrese el número de la respuesta correcta '))
            validar_respuesta_iterator = filter(
                lambda respuesta: respuesta.numero_respuesta == a, filtroaleatorio[0].respuestas)
            filtered = list(validar_respuesta_iterator)

            if (len(filtered) > 0):
                print("respuesta elegidad ", a)
            else:
                print("opción no exitente")

            # Prueba de respuesta correcta
            if (filtered[0].es_correcta == True):
                # print("respuesta correcta ")
                nivel_actual = +1
            else:
                jugador_activo = False
                # print("respuesta incorrecta")
                # print(jugador_activo)
                nombre_jugador = input("ingrese el nombre del jugador ") 
else:
    print("final del juego")
    print(nivel_actual)


